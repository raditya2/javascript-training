function Student()
{
    this.details = [];

    this.addDetails = (singleRecord) => {
        return new Promise((resolve, reject)=>{
            
            let data  = JSON.parse(singleRecord);
            
            if(data.name && data.email)
            {
                this.details.push( data )
                resolve(data);
            }
            else
            {
                reject(new Error('Error in data insertion.'));
            }
        });
    }

    this.updateDetails = (id, singleRecord) => {
        return new Promise((resolve, reject)=>{

            if(typeof id === 'undefined')
            {
                reject(new Error('ID require for Updation.'));
            }
            else 
            {
                if(typeof this.details[id] === 'undefined') 
                {
                    reject(new Error('Given ID not exist.'));
                }
                else 
                {
                    if (id > -1)
                    {
                        if(!singleRecord)
                        {
                            reject(new Error('Data missing for updation.'));
                        }
                        else
                        {
                            let data  = JSON.parse(singleRecord);

                            if(data.name)
                            {
                                this.details[id]['name'] = data.name;
                            }
                        
                            if(data.email)
                            {
                                this.details[id]['email'] = data.email;
                            }

                            resolve(data);
                        }
                    }
                    else
                    {
                        reject(new Error('Invalid ID.'));
                    }
                }
            }
        });
    }

    this.deleteDetails = (id) => {
        return new Promise((resolve, reject)=>{
            if(typeof id === 'undefined')
            {
                reject(new Error('ID require for Deletion.'));
            }
            else 
            {
                if(typeof this.details[id] === 'undefined') 
                {
                    reject(new Error('Given ID not exist.'));
                }
                else 
                {
                    if (id > -1)
                    {
                        this.details.splice(id, 1);
                        resolve(id);
                    }
                    else
                    {
                        reject(new Error('Invalid ID.'));
                    }
                }
            }
        });
    }

    this.specificDetails = (id) => {
        return new Promise((resolve, reject)=>{
            if(typeof id === 'undefined')
            {
                reject(new Error('ID require for Selection.'));
            }
            else 
            {
                if(typeof this.details[id] === 'undefined') 
                {
                    reject(new Error('Given ID not exist.'));
                }
                else 
                {
                    if (id > -1)
                    {
                        resolve(this.details[id]);
                    }
                    else
                    {
                        reject(new Error('Invalid ID.'));
                    }
                }
            }
        });
    }

    this.listDetails = () => {
        return new Promise((resolve, reject)=>{
            if(this.details.length==0)
            {
                reject(new Error('Data not found.'));
            }
            else
            {
                this.details.forEach( item => { 
                    return console.log(item.name, item.email);
                });
                resolve('listed successfully');
            }
        });
    }
}

function Department()
{
    this.details = [];

    this.addDetails = (singleRecord) => {
        return new Promise((resolve, reject)=>{
            
            let data  = JSON.parse(singleRecord);
            
            if(data.name && data.email)
            {
                this.details.push( data )
                resolve(data);
            }
            else
            {
                reject(new Error('Error in data insertion.'));
            }
        });
    }

    this.updateDetails = (id, singleRecord) => {
        return new Promise((resolve, reject)=>{

            if(typeof id === 'undefined')
            {
                reject(new Error('ID require for Updation.'));
            }
            else 
            {
                if(typeof this.details[id] === 'undefined') 
                {
                    reject(new Error('Given ID not exist.'));
                }
                else 
                {
                    if (id > -1)
                    {
                        if(!singleRecord)
                        {
                            reject(new Error('Data missing for updation.'));
                        }
                        else
                        {
                            let data  = JSON.parse(singleRecord);

                            if(data.name)
                            {
                                this.details[id]['name'] = data.name;
                            }
                        
                            if(data.email)
                            {
                                this.details[id]['email'] = data.email;
                            }

                            resolve(data);
                        }
                    }
                    else
                    {
                        reject(new Error('Invalid ID.'));
                    }
                }
            }
        });
    }

    this.deleteDetails = (id) => {
        return new Promise((resolve, reject)=>{
            if(typeof id === 'undefined')
            {
                reject(new Error('ID require for Deletion.'));
            }
            else 
            {
                if(typeof this.details[id] === 'undefined') 
                {
                    reject(new Error('Given ID not exist.'));
                }
                else 
                {
                    if (id > -1)
                    {
                        this.details.splice(id, 1);
                        resolve(id);
                    }
                    else
                    {
                        reject(new Error('Invalid ID.'));
                    }
                }
            }
        });
    }

    this.specificDetails = (id) => {
        return new Promise((resolve, reject)=>{
            if(typeof id === 'undefined')
            {
                reject(new Error('ID require for Selection.'));
            }
            else 
            {
                if(typeof this.details[id] === 'undefined') 
                {
                    reject(new Error('Given ID not exist.'));
                }
                else 
                {
                    if (id > -1)
                    {
                        resolve(this.details[id]);
                    }
                    else
                    {
                        reject(new Error('Invalid ID.'));
                    }
                }
            }
        });
    }

    this.listDetails = () => {
        return new Promise((resolve, reject)=>{
            if(this.details.length==0)
            {
                reject(new Error('Data not found.'));
            }
            else
            {
                this.details.forEach( item => { 
                    return console.log(item.name, item.email);
                });
                resolve('listed successfully');
            }
        });
    }
}

// function Students()
// {
//     CURD_Operations.call(this);
// }

// function Departments()
// {
//     CURD_Operations.call(this);
// }
// *******************************************************************
let stu = new Student();
stu.addDetails('{"name": "rakesh", "email": "raditya@deqode.com"}')
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );

stu.addDetails('{"name": "om", "email": "omprakash@deqode.com"}')
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );

stu.addDetails('{"name": "nitin", "email": "ntiwari@deqode.com"}')
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );
console.log(stu.details);

stu.updateDetails(2, '{"name": "aman", "email": "ayadav@deqode.com"}')
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );
console.log(stu.details);

stu.deleteDetails(0)
    .then (value => console.log(value) )
    .catch( error => console.error(error.message) );
console.log(stu.details);

stu.specificDetails(0)
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );
console.log(stu.details);

stu.listDetails()
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );
// *******************************************************************
// *******************************************************************
let dep = new Department();
dep.addDetails('{"name": "dep1", "email": "dep1@deqode.com"}')
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );

dep.addDetails('{"name": "dep2", "email": "dep2@deqode.com"}')
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );

dep.addDetails('{"name": "dep3", "email": "dep3@deqode.com"}')
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );
console.log(dep.details);

dep.updateDetails(0, '{"name": "dep4", "email": "dep4@deqode.com"}')
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );
console.log(dep.details);

dep.deleteDetails(1)
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );
console.log(dep.details);

dep.specificDetails(1)
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );
console.log(dep.details);

dep.listDetails()
    .then( value => console.log(value) )
    .catch( error => console.error(error.message) );
// *******************************************************************