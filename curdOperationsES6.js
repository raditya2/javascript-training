class Student
{
    constructor()
    {
        this.details = [];
    }

    addDetails(singleRecord)
    {
        let data  = JSON.parse(singleRecord);
        
        if(data.name && data.email)
        {
            this.details.push( data );
        }
        else
        {
            throw new Error('Please give all the Details.');
        }
    }

    updateDetails(id, singleRecord)
    {
        if(typeof id === 'undefined')
        {
            throw new Error('ID require for Updation.');
        }
        else 
        {
            if(typeof this.details[id] === 'undefined') 
            {
                throw new Error('Given ID not exist.');
            }
            else 
            {
                if (id > -1)
                {
                    if(!singleRecord)
                    {
                        throw new Error('Data missing for updation.');
                    }
                    else
                    {
                        let data  = JSON.parse(singleRecord);

                        if(data.name)
                        {
                            this.details[id]['name'] = data.name;
                        }
                    
                        if(data.email)
                        {
                            this.details[id]['email'] = data.email;
                        }
                    }
                }
                else
                {
                    throw new Error('Invalid ID.');
                }
            }
        }
    }

    deleteDetails(id)
    {
        if(typeof id === 'undefined')
        {
            throw new Error('ID require for Deletion.');
        }
        else 
        {
            if(typeof this.details[id] === 'undefined') 
            {
                throw new Error('Given ID not exist.');
            }
            else 
            {
                if (id > -1)
                {
                    this.details.splice(id, 1);
                }
                else
                {
                    throw new Error('Invalid ID.');
                }
            }
        }
    }

    specificDetails(id)
    {
        if(typeof id === 'undefined')
        {
            throw new Error('ID require for Selection.');
        }
        else 
        {
            if(typeof this.details[id] === 'undefined') 
            {
                throw new Error('Given ID not exist.');
            }
            else 
            {
                if (id > -1)
                {
                    console.log(this.details[id]);
                }
                else
                {
                    throw new Error('Invalid ID.');
                }
            }
        }
    }

    listDetails()
    {
        this.details.forEach( item => { 
            return console.log(item.name, item.email);
        });
    }
}

// class Students extends curdOperations
// {
//     constructor()
//     {
//         super();
//     }
// }

let stu = new Student();

try
{
    stu.addDetails('{"name": "rakesh", "email": "raditya@deqode.com"}');
    stu.addDetails('{"name": "om", "email": "omprakash@deqode.com"}');
    stu.addDetails('{"name": "nitin", "email": "ntiwari@deqode.com"}');

    console.log(stu.details);
    stu.listDetails();
    stu.deleteDetails(0);
    console.log(stu.details);
    stu.updateDetails(1, '{"name": "aman", "email": "ayadav@deqode.com"}');
    console.log(stu.details);
    stu.specificDetails(1);
    stu.listDetails();
}
catch(err)
{
    console.error("Invalid data: " + err.message);
} 

// class Departments extends curdOperations
// {
//     constructor()
//     {
//         super();
//     }
// }

class Department
{
    constructor()
    {
        this.details = [];
    }

    addDetails(singleRecord)
    {
        let data  = JSON.parse(singleRecord);
        
        if(data.name && data.email)
        {
            this.details.push( data );
        }
        else
        {
            throw new Error('Please give all the Details.');
        }
    }

    updateDetails(id, singleRecord)
    {
        if(typeof id === 'undefined')
        {
            throw new Error('ID require for Updation.');
        }
        else 
        {
            if(typeof this.details[id] === 'undefined') 
            {
                throw new Error('Given ID not exist.');
            }
            else 
            {
                if (id > -1)
                {
                    if(!singleRecord)
                    {
                        throw new Error('Data missing for updation.');
                    }
                    else
                    {
                        let data  = JSON.parse(singleRecord);

                        if(data.name)
                        {
                            this.details[id]['name'] = data.name;
                        }
                    
                        if(data.email)
                        {
                            this.details[id]['email'] = data.email;
                        }
                    }
                }
                else
                {
                    throw new Error('Invalid ID.');
                }
            }
        }
    }

    deleteDetails(id)
    {
        if(typeof id === 'undefined')
        {
            throw new Error('ID require for Deletion.');
        }
        else 
        {
            if(typeof this.details[id] === 'undefined') 
            {
                throw new Error('Given ID not exist.');
            }
            else 
            {
                if (id > -1)
                {
                    this.details.splice(id, 1);
                }
                else
                {
                    throw new Error('Invalid ID.');
                }
            }
        }
    }

    specificDetails(id)
    {
        if(typeof id === 'undefined')
        {
            throw new Error('ID require for Selection.');
        }
        else 
        {
            if(typeof this.details[id] === 'undefined') 
            {
                throw new Error('Given ID not exist.');
            }
            else 
            {
                if (id > -1)
                {
                    console.log(this.details[id]);
                }
                else
                {
                    throw new Error('Invalid ID.');
                }
            }
        }
    }

    listDetails()
    {
        this.details.forEach( item => { 
            return console.log(item.name, item.email);
        });
    }
}

let dep = new Department();

try
{
    dep.addDetails('{"name": "dep1", "email": "dep1@deqode.com"}');
    dep.addDetails('{"name": "dep2", "email": "dep2@deqode.com"}');
    dep.addDetails('{"name": "dep3", "email": "dep3@deqode.com"}');

    console.log(dep.details);
    dep.listDetails();
    dep.deleteDetails(0);
    console.log(dep.details);
    dep.updateDetails(1, '{"name": "dep4", "email": "dep4@deqode.com"}');
    console.log(dep.details);
    dep.specificDetails(1);
    dep.listDetails();
}
catch(err)
{
    console.error("Invalid data: " + err.message);
} 