function getCounter(num)
{
    let count = 0;

    return () => { 
        count++;
        console.log(num*count);
    }
}

const counter = getCounter(5);
counter() // 5
counter() // 10
counter() // 15
counter() // 20
counter() // 25