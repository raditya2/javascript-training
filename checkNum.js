function checkNum(val, callback)
{
    if(val<0)
    {
        callback(new Error('Number is negative'), null);
    }
    else
    {
        callback(null, {message:'Number is not negative'});
    }
}

checkNum(0, (err, result)=>{
    if(err)
    {
        console.error(err.message);
    }
    else
    {
        console.log(result.message);
    } 
});