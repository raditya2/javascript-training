function checkNumWrapper(num) {
    return new Promise(function (resolve, reject) {
        if (isNaN(num)) {
            reject(new Error('Not a Number'));
        }
        else {
            checkNum(num, function(err, result){
                if(err) {
                    reject(err)
                }
                resolve(result)
            });
        }
    });
}

function checkNum(val, callback)
{
    if(val<0)
    {
        callback(new Error('Number is negative'), null);
    }
    else
    {
        callback(null, {message:'Number is not negative'});
    }
}

// let onfulfilment = (result) => {
//     console.log(result);
// }

// let onrejection = (error) => {
//     console.log(error);
// }

checkNumWrapper(-1).then(result => {
    console.log(`Result : ${result.message}`)
}).catch(error => {
    console.error(`Error Message : ${error.message}`);
});