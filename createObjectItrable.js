const person = {
    name: 'John',
    age: 30,
    country: 'U.S',
};

// person[Symbol.iterator] = () => {
//     for (let key of Object.keys(person)) {
//         yield person[key];
//     }
// };

person[Symbol.iterator] = function () {
    let values = Object.values(this);
    let index = 0;
    return {
        // next() {
        //     let val = values[index];
        //     index++;
        //     return { value: val, done: index <= (values.length) ? false : true }
        // }
        next: () => ({ done: (values.length)===index, value: values[index++] })
    }
};

for (const value of person) {
    console.log(`value : ${value}`);
}